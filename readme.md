
Kata TDD 001- Python

Objetivo

Implementar una calculadora de Strings [Kata propuesta por Roy Osherove] (http://osherove.com/tdd-kata-1/)
Primera versión:

La calculadora tiene un metodo sumar que recibe un string vacio, un string con un número o un string con dos numeros separados por coma.

Condiciones de la Kata - Solo se prueba para datos de entrada válidos
Paso 1 - Que funcione para cadena vacia

    (Rojo) Crear una prueba para cuando la cadena es vacia
    (Verde) Se escribe el método para que la prueba corra

Paso 2 - Que funcione para cadena con un numero (pruebas (1) un numero (2) que funcione con dos distintos)

    (Rojo) Se agrega una prueba para cuando la cadena trae un numero
    (Verde) Se corrige el código para que corra la prueba
    (Rojo) Se agrega prueba para probar con otro numero diferente
    (Verde) Se corrige el código para que la prueba corra

Paso 3 - Que funcione para caso de cadena con dos numeros

    (Rojo) Se agrega una prueba para cuando la cadena trae dos numeros
    (Verde) Se corrige el codigo para que la prueba corra
    (Verde) Se agrega otra prueba y se hace refactoring del codigo

Paso 4

    listado de pruebas

        Prueba suma con tres números
        Prueba suma con cinco números
        Prueba suma con siete números
        prueba suma con diez números

    (Rojo) Se agregan las pruebas previamente contenidas en el listado
    (Verde) Se corrige el codigo para que las pruebas listadas anteriormente corran y pasen
    (Verde) Pequeño refactoring de code


Paso 5

    listado de pruebas

        Prueba suma con tres números con separador &
        Prueba suma con cinco números con separador &
        Prueba suma con siete números con separador &
        prueba suma con diez números con separador &
        Prueba suma con tres números con separador :
        Prueba suma con cinco números con separador :
        Prueba suma con siete números con separador :
        prueba suma con diez números con separador :


    (Rojo) Se agregan las pruebas previamente contenidas en el listado
    (Verde) Se corrige el codigo para que las pruebas listadas anteriormente corran y pasen
    (Verde) Se hace un pequeño refactor eliminando prints en el codigo