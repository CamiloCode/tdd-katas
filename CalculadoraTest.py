__author__ = 'omarcontreras'


import unittest
from Calculadora import Calculadora


class CalculadoraTestCase(unittest.TestCase):
    def test_sumar_vacia(self):

        self.assertEqual(Calculadora().sumar(""),0,"Cadena vacia")

    def test_sumar_cadenaConUnNumero(self):
        self.assertEqual(Calculadora().sumar("1"),1,"Un numero")
        self.assertEqual(Calculadora().sumar("2"),2,"Un numero")

    def test_sumar_cadenaDosNumeros(self):
        self.assertEqual(Calculadora().sumar("1,2"),3,"dos numeros")
        self.assertEqual(Calculadora().sumar("4,5"),9,"dos numeros")

    def test_sumar_cadenaTresNumeros(self):
        self.assertEqual(Calculadora().sumar("1,2,3"),6,"tres numeros")
        self.assertEqual(Calculadora().sumar("4,5"),9,"dos numeros")

    def test_sumar_cadenaCincoNumeros(self):
        self.assertEqual(Calculadora().sumar("1,2,3,4,5"),15,"cinco numeros")
        self.assertEqual(Calculadora().sumar("4,5"),9,"dos numeros")

    def test_sumar_cadenaSieteNumeros(self):
        self.assertEqual(Calculadora().sumar("1,2,3,4,5,6,7"),28,"siete numeros")
        self.assertEqual(Calculadora().sumar("4,5"),9,"dos numeros")

    def test_sumar_cadenaDiezNumeros(self):
        self.assertEqual(Calculadora().sumar("1,2,3,4,5,6,7,8,9,10"),55,"diez numeros")
        self.assertEqual(Calculadora().sumar("4,5"),9,"dos numeros")

    def test_sumar_cadenaTresNumeros_separadorAmpersand(self):
        self.assertEqual(Calculadora().sumar("1&2&3"),6,"tres numeros con ampersand")
        self.assertEqual(Calculadora().sumar("4&5"),9,"dos numeros con ampersand")

    def test_sumar_cadenaCincoNumeros_separadorAmpersand(self):
        self.assertEqual(Calculadora().sumar("1&2&3&4&5"),15,"cinco numeros con ampersand")
        self.assertEqual(Calculadora().sumar("4&5"),9,"dos numeros con ampersand")

    def test_sumar_cadenaSieteNumeros_separadorAmpersand(self):
        self.assertEqual(Calculadora().sumar("1&2&3&4&5&6&7"),28,"siete numeros con ampersand")
        self.assertEqual(Calculadora().sumar("4&5"),9,"dos numeros con ampersand")

    def test_sumar_cadenaDiezNumeros_separadorAmpersand(self):
        self.assertEqual(Calculadora().sumar("1&2&3&4&5&6&7&8&9&10"),55,"diez numeros con ampersand")
        self.assertEqual(Calculadora().sumar("4&5"),9,"dos numeros con ampersand")

    def test_sumar_cadenaTresNumeros_separadorDosPuntos(self):
        self.assertEqual(Calculadora().sumar("1:2:3"),6,"tres numeros con dos puntos")
        self.assertEqual(Calculadora().sumar("4:5"),9,"dos numeros con dos puntos")

    def test_sumar_cadenaCincoNumeros_separadorDosPuntos(self):
        self.assertEqual(Calculadora().sumar("1:2:3:4:5"),15,"cinco numeros con dos puntos")
        self.assertEqual(Calculadora().sumar("4:5"),9,"dos numeros con dos puntos")

    def test_sumar_cadenaSieteNumeros_separadorDosPuntos(self):
        self.assertEqual(Calculadora().sumar("1:2:3:4:5:6:7"),28,"siete numeros con dos puntos")
        self.assertEqual(Calculadora().sumar("4:5"),9,"dos numeros con dos puntos")

    def test_sumar_cadenaDiezNumeros_separadorDosPuntos(self):
        self.assertEqual(Calculadora().sumar("1:2:3:4:5:6:7:8:9:10"),55,"diez numeros con dos puntos")
        self.assertEqual(Calculadora().sumar("4:5"),9,"dos numeros con dos puntos")

if __name__ == '__main__':
    unittest.main()